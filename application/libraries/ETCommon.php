<?php

/**
 * @package ETCommon Library
 *
 * @author 	E-Tekkies Team
 * @version 1.0
 * @since 	version 0.1
 */

class ETCommon
{
    var $CI;

    function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->helper('email');
        $this->CI->load->helper('string');
	}
	
    function ETCommon(){
        self::__construct();
    }
	
	/**
	 * This function 'll validate url format
	 *
	 * @access public
	 * @param String $URL current url 
	 * @return boolean 
	 */

    function validateURL($URL) {
		$v = "/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i";
		return (bool)preg_match($v, $URL);
	}
	
	/**
	 * This function 'll validate date as 'dd/mm/yyyy'
	 *
	 * @access public
	 * @param String $date 
	 * @return true or false
	 */
	
	function isValidDate($date){
		if(preg_match("/^(\d{2})-(\d{2})-(\d{4})$/", $date, $matches)){
			if(checkdate($matches[2], $matches[1], $matches[3])){
				return true;
			}else
			  return false;
		}else{
			return false;	
		}
	}
	
	/**
	 * This function 'll format date in dd-mm-yy to date as 'yyyy-mm-dd'
	 *
	 * @access public
	 * @param String $date 
	 * @return date string
	 */
	 
	function formatDatetoTimestamp($date){
		$date_split=explode('/',$date);
		$mm=isset($date_split[1])?$date_split[1]:'00';
		$dd=isset($date_split[0])?$date_split[0]:'00';
		$yy=isset($date_split[2])?$date_split[2]:'0000';
		$time_now=mktime(date('h')+4,date('i')+30,date('s'));
		$time =date('h:i:s',$time_now);
		return $yy.'-'.$mm.'-'.$dd.'-'.$time;
	}
	
	/**
	 * This function 'll format date in dd-mm-yy to date as 'yyyy-mm-dd'
	 *
	 * @access public
	 * @param String $date 
	 * @return date string
	 */
	 
	function getDbDateForm($date){
		$date_split=explode('-',$date);
		$mm=isset($date_split[1])?$date_split[1]:'00';
		$dd=isset($date_split[0])?$date_split[0]:'00';
		$yy=isset($date_split[2])?$date_split[2]:'0000';
		return $yy.'-'.$mm.'-'.$dd;
		
	}
	
	/**
	 * This function 'll format date in yyyy-mm-dd to date as 'dd-mm-yyyy'
	 *
	 * @access public
	 * @param String $date 
	 * @return date string
	 */
	 
	function getUserDateForm($date){
		$date_split=explode('-',$date);
		$mm=$date_split[1];
		$dd=$date_split[2];
		$yy=$date_split[0];
		
		//if date not correct (in case of '00-00-0000') return false
		if($dd=='00')
			return false;
		else
			return $dd.'-'.$mm.'-'.$yy;
		
	}
	
	/**
	 * This function 'll format date in Y-m-d H:i:s to date as 'dd/mm/yyyy'
	 *
	 * @access public
	 * @param String $date 
	 * @return date string
	 */
	 
	function formatDateTimeToClient($date){
		
		$date_time_split	=	explode(' ',$date);
		$ndate				=	$date_time_split[0];
		$time_str			=	$date_time_split[1];		
				
		$date_split=explode('-',$ndate);
		$mm					=	$date_split[1];
		$dd					=	$date_split[2];
		$yy					=	$date_split[0];
		
		//if date not correct (in case of '00-00-0000') return false
		if($dd	==	'00')
			return false;
		else
			return $dd.'/'.$mm.'/'.$yy.' '.$time_str;
		
	}
	
	// -----------------------------------------------------------------------------------------
	
	
	
}

/* End of file ETClasses.php */
/* Location: ./application/libraries/ETClasses.php */