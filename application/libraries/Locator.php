<?php
include("application/third_party/geoiplocator/src/GeoIPLocator.class.php");
class Locator{
	var $CI;
	function get_location(){
		$location = array();
		$this->CI =& get_instance();
		$ip_address = $this->CI->input->ip_address();
		$geoip = new GeoIPLocator($ip_address);
		$info = $geoip->getIPInfo();
		
		$country = $info->getCountryName();
		if(!$country){$country = "";}
		
		$location['ip'] = $ip_address;
		$location['country_name'] = $country;
		$location['city_name'] = '';
		return $location;
		
	}

}
?>