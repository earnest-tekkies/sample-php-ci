<?php
/*
* @Package Quick Authentication Library
* @author David Blencowe
* @link http://www.syntaxmonster.net
* @version 1.0.0
* @since Version 1.0.0
*/
class Quickauth
{
    var $CI;
    var $_username;
	var $_language=1;// global variable for handling language
    var $_table = array(
                    'users' => 'sm_users',
                    'groups' => 'sm_user_groups'
                    );

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->helper('email');
        $this->CI->load->helper('string');
	}

	function is_maintain(){
		$setting =  $this->CI->db->get_where('tbl_config', array('maintenance_enabled' => 1))->row_object();
		if(is_object($setting) && $setting->maintenance_enabled == 1){
			redirect('maintenance');
		}
	}
	
	function is_maintain_mode(){
		$setting =  $this->CI->db->get_where('sm_config', array('maintenance_enabled' => 1))->row_object();
		
		if(is_object($setting) && $setting->maintenance_enabled == 1){
			return true;
		}else{
			return false;	
		}
	}
	
    function Quickauth()
    {
        self::__construct();
    }
	function has_role($only_for_administrator = true, $only_for_user = false){
		if ($this->CI->session->userdata('logged_in') == FALSE)
		{
			redirect('login');
		}
		else
		{	
			if($this->CI->session->userdata('group_id') != 3){
				if($only_for_administrator){
					redirect('membros');	
				}
			}else if($only_for_user){
				redirect('admin/pages');	
			}
		}
	}
		
	function is_admin(){
		if($this->CI->session->userdata('group_id') == 1){
			return true;	
		}else{
			return false;
		}
	}
	
	function is_supportor(){
		if($this->CI->session->userdata('group_id') == 2){
			return true;	
		}else{
			return false;
		}
	}	
	
	function redirect_out_admin(){
		redirect(base_url().'user/login');
	}
	
	// -----------------------------------------------------------------------------------------
	
	/**
    * Check the database too see if the user email that is passed to the function exists.
    * @param String $email
	* @param Integer $user_id , by default it will be zero
    * @return TRUE/FALSE
    */
    function _useremail_exists($email,$user_id=0){
        $this->CI->db->where('email', $email);
		//if user id provided
		if($user_id > 0){
			$this->CI->db->where('id <>', $user_id);
		}
        $this->CI->db->limit(1);
        $query = $this->CI->db->get($this->_table['users']);

        if ($query->num_rows() !== 1){
            return FALSE;
        }else{
			return TRUE;
        }
    }
	
	// -----------------------------------------------------------------------------------------
	
	/**
    * Check the config file to see if the current language.
    * If it does then it is set to the global $_username variable for later use
    * 
    * @return $lan_id
    */	
	function _get_language_code(){
		$lan_id		=	0;
		$cur_lanhuage=$this->CI->config->item('admin_language');
		if($cur_lanhuage	==	'english'){
			$lan_id				=	1;
			$this->_language	=	$lan_id;
		}else if($cur_lanhuage	==	'portuguese'){
			$lan_id				=	2;
			$this->_language	=	$lan_id;
		}
		
		return $lan_id;
		
	}
	
	// -----------------------------------------------------------------------------------------
	
    /**
    * Used for restricting users to certain controllers and functions
    * by their user level.
    * @param String $restrict_to Name of the group
    * @return TRUE/Error
    */
    function restrict( $restrict_to = NULL, $redirect_to_login = FALSE )
    {
        if ( $restrict_to !== NULL)
        {
            if ($this->CI->session->userdata('logged_in') == TRUE)
            {
                $this->CI->db->where('id', $restrict_to);
                $query = $this->CI->db->get($this->_table['groups']);
                $level = $query->row_array();
                $users_level = $this->CI->session->userdata('group_id');

                if ($users_level >= $level['id'])
                {
                    return TRUE;
                }
                else
                {
                    redirect('/admin/');
                }
            }
            else
            {
               redirect("/admin/");
            }
        }
        else
        {
            // Page locked to everyone
            show_error($this->CI->lang->line('access_denied'));
        }
    }
	
	// -----------------------------------------------------------------------------------------

    /**
    * Check the database too see if the username that is passed to the function exists.
    * If it does then it is set to the global $_username variable for later use
    * @param String $username
    * @return TRUE/FALSE
    */
    function _username_exists( $username )
    {
        $this->CI->db->where('username', $username);
        $this->CI->db->limit(1);
        $query = $this->CI->db->get($this->_table['users']);

        if ($query->num_rows() !== 1)
        {
            return FALSE;
        }
        else
        {
            $this->_username = $username;
            return TRUE;
        }
    }
	
	// -----------------------------------------------------------------------------------------

    /**
    * Encrypts the submitted password and then checks it in the database
    * using the value of the global $_username variable. If True is returned
    * then the username and password submitted by the user are correct and they
    * should then get logged in (See login function)
    * @param String $password
    * @return TRUE/FALSE
    */
    function _check_correct_password( $password )
    {
        $this->CI->db->select('password');
        $this->CI->db->where('username', $this->_username);
        $this->CI->db->limit(1);
        $query = $this->CI->db->get($this->_table['users']);
        $result = $query->row();

        if ($result->password == $this->encrypt($password))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
	
	// -----------------------------------------------------------------------------------------

    /**
    * Returns the number of characters in a string after it has been trimemd for
    * whitespace.
    * @param String $string
    * @return Int
    */
    function check_string_length( $string )
    {
        $string = trim($string);
        return strlen($string);
    }
	
	// -----------------------------------------------------------------------------------------

    /**
    * This function will encrypt any data passed to it.
    * It is primarily used for encrypting passwords before
    * querying the database.
    * @param String $data
    * @return String
    */
    function encrypt( $data )
    {
        if ($this->CI->config->item('encryption_key') !== NULL)
        {
            return sha1($this->CI->config->item('encryption_key').$data);
        }
        else
        {
            show_error('Please set an encryption key in your config file. <a href="javascript:history.back();">back</a>');
        }
    }
	
	// -----------------------------------------------------------------------------------------

    /**
    * Check if a user is logged in
    *
    * @access public
    * @param string
    * @return bool
    */
    function logged_in()
    {
        return $this->CI->session->userdata('logged_in');
    }
	
	// -----------------------------------------------------------------------------------------

    /**
    * Log a user out (destroy all session variables)
    *
    * @access public
    */
    function logout()
    {
		$this->CI->session->sess_destroy();
    }
	
	// -----------------------------------------------------------------------------------------

    /**
    * This function will get the currently selected template theme
    *
    * @access public
	* @return string template name
    */
    function get_current_template_css(){
		$template_id	=	0;
		$this->CI->db->select('template');
        $query = $this->CI->db->get('sm_config');
        $result = $query->row();
		// check the sm_config for seleted template if template set the get template id from there else set template is as 1
		if ($query->num_rows() !== 1){
			$template_id	=	1;
		}else{
			$template_id	=	$result->template;
		}
		
		// if template id is zero then make it as one for getting default template 		
		if($template_id < 1){
			$template_id	=	1;			
		}
		
		//get the template name from table sm_template_css
		$this->CI->db->select('template_css');
		$this->CI->db->where('id', $template_id);
        $query = $this->CI->db->get('sm_template_css');
        $result = $query->row();
		if ($query->num_rows() !== 1){
			return 'none';
		}else{
			return $result->template_css;
		}		
    }
	
	 /**
    * This function will get the currently  copy right text 
    *
    * @access public
	* @return string copy right text 
    */
    function get_copy_right_text(){
		$language_id	=	$this->_get_language_code();
		$this->CI->db->select('copy_right_text');
		$this->CI->db->where('language', $language_id);
        $query	=	$this->CI->db->get('sm_config');
        $result	=	$query->row();
		if ($query->num_rows() !== 1){
			return 'none';
		}else{
			return $result->copy_right_text;
		}		
    }
	
}

/* End of file Quickauth.php */
/* Location: ./application/libraries/Quickauth.php */