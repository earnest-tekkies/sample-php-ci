<?php
#Global
#Title
$lang['pgt_index'] 		= "CSI SKD";
$lang['pgt_prefix']		= "CSI SKD :: ";
$lang['user_login_h1'] 	= "SOUTH KERALA DIOCESE (CSITA)";

#Login
$lang['login'] 			= "login";
$lang['pgt_index'] 		= "CSI SKD";
$lang['name']			= "Name";
$lang['email']			= "Email";
$lang['password']		= "Password";
$lang['welcome'] 		= "Welcome";
$lang['access'] 		= "Login";
$lang['logout'] 		= "Logout";

#Recept
$lang['received_from'] 	= "Received from";
$lang['sum_of_rupees'] 	= "Sum of Rupees";
$lang['payment_mode'] 	= "Payment mode";
$lang['cheque_number']	= "Cheque number";
$lang['cheque_of']		= "Cheque of";
$lang['cheque_date']	= "Cheque date";
$lang['on_account_of']	= "On account of";
$lang['process']		= "Process";
$lang['receipt_date']	= "Receipt Date";

