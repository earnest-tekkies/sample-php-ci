<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php echo $admin_page_title; ?></title>
	<link href="<?php echo $this->config->item('admin_assets_path');?>css/main.css" rel="stylesheet" type="text/css" />
	
	<!-- Jquery Latest -->
	<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
    <!--tooltip preview-->
    <script src="<?php echo $this->config->item('admin_assets_path');?>tooltip/tool_tip.js"  type="text/javascript"></script>
        
    <!-- Bootstrap JS -->
	<script src="<?php echo $this->config->item('admin_assets_path');?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    
    <!--  File Upload CSS -->
	<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets_path');?>theme/beautyadmin/js/jquery-file-upload/css/jquery.fileupload-ui.css">
    <script type="text/javascript">
		base_url	=	"<?php echo base_url();?>";
	</script>
    
</head>
<body class="main">
	<div id="contents"><?= $content ?></div>
</body>
</html>
<!-- Localized -->