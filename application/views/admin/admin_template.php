<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php echo $admin_page_title;//$this->lang->line('admin_page_title'); ?></title>
	<link href="<?php echo $this->config->item('admin_assets_path');?>css/main.css" rel="stylesheet" type="text/css" />
	
	<!-- Jquery Latest -->
	<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
    <!--tooltip preview-->
    <script src="<?php echo $this->config->item('admin_assets_path');?>tooltip/tool_tip.js"  type="text/javascript"></script>
    <!-- Jw Player for videos_view page -->
    <script type="text/javascript" src="<?php echo $this->config->item('admin_assets_path');?>jwplayer/jwplayer.js" ></script>
	<script type="text/javascript">jwplayer.key="S8KbJ2nr7bCfQfjOE5opq82DqnNGlMO5MjlfKw==";</script>
    
    <!-- Bootstrap JS -->
	<script src="<?php echo $this->config->item('admin_assets_path');?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    
    <!--  File Upload CSS -->
	<link rel="stylesheet" href="<?php echo $this->config->item('admin_assets_path');?>theme/beautyadmin/js/jquery-file-upload/css/jquery.fileupload-ui.css">
    <script type="text/javascript">
		base_url	=	"<?php echo base_url();?>";
	</script>
    
</head>
<body class="main">
	<!--  Header -->
		<table width="1024" border="0" align="center" cellpadding="0" cellspacing="0">
		  <tr>
		    <td height="63" align="center" bgcolor="#666666"><p style="font-size: 24px; font-weight: bold; color: #CCC;"><?php echo $this->lang->line('user_login_h1'); ?></p></td>
		  </tr>
		  <tr>
		    <td height="36" align="center" bgcolor="#FFFFFF">
		    	<div style="border:solid 1px #999; background-color:#CCC; padding:4px; float:left;"><a href="<?php echo base_url().'admin/new-recept'?>">Add Recept</a></div>
		    	<div style="border:solid 1px #999; background-color:#CCC; padding:4px; float:left;">Add B-forms</div>
		    	<div style="border:solid 1px #999; background-color:#CCC; padding:4px; float:left;"><a href="pre-filled-b-foms.html">Pre-filled B-forms</a></div>
		    	<div style="border:solid 1px #999; background-color:#CCC; padding:4px; float:left;"><a href="<?php echo base_url().'admin/report'?>">Report</a></div>
		    	<div style="border:solid 1px #999; background-color:#CCC; padding:4px; float:left;"><a href="<?php echo base_url();?>logout"><?php echo $this->lang->line('logout'); ?></a></div>
		    
		    	<span class="welcome"><?php echo $this->lang->line('welcome'); ?>
						, <b><?php echo $this->session->userdata('username');?></b>!
				</span>
			</td>
		  </tr>
		
		<!-- End Main Menu -->
	<!-- End Header -->

	<!-- Start Content -->
	<tr><td bgcolor="#FFFFFF">
	<div id="contents"><?= $content ?></div>
</td>
	</tr>
	<!-- End Content  -->

	<!-- Footer -->
	<tr>
	</tr>	
	<!--  End Footer -->

</table>
</body>
</html>
<!-- Localized -->