<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content=""/>
<meta name="keywords" content=""/>

<title><?php echo $this->lang->line('pgt_index');?></title>

<script type="text/javascript" src="<?php echo $this->config->item('admin_assets_path');?>js/jquery-1.6.3.min.js"></script>

<link href="<?php echo $this->config->item('admin_assets_path');?>css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div>
  <?php
    $attibuts= array('id' =>  'loginForm',
                    'name' =>  'loginForm');
    echo form_open('index/login',$attibuts);?>

<table width="1024" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="63" align="center" bgcolor="#666666"><p style="font-size: 24px; font-weight: bold; color: #CCC;"><?php echo $this->lang->line('user_login_h1'); ?> </p></td>
  </tr>
  <tr>
    <td height="36" align="center" bgcolor="#FFFFFF">

<?php if($error) {?><span class="erro bg"><?php echo $error_message;?></span><?php } ?>
<?php if(validation_errors()) {?><span class="erro bg"><?php echo validation_errors(); ?></span><?php }?>

</td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="500" border="0" align="center" cellpadding="0" cellspacing="0" style="text-align:left;">
      <tr>
        <td width="247" class="ssssss">
          <label for="email"><?php echo $this->lang->line('email'); ?></label>
        </td>
        <td width="253">
          <?php 
            $data = array('name'    => 'username',
                          'value'   => set_value('username'),
                          'id'      => 'username'
                        );
            echo form_input($data); ?>
        </td>
      </tr>
      <tr>
        <td>
          <label for="password"><?php echo $this->lang->line('password'); ?></label>
        </td>
        <td> 
          <?php 
            $data = array('name'   => 'password',
                          'id'     => 'password'
                        );
            echo form_password($data); 
          ?>
                    
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
        <?php 
          $data = array('name'        => 'login',
                        'id'          => 'login',
                        'value'    =>$this->lang->line('login'),
                        'class'       => 'botao');
          echo form_submit($data); 
        ?>
                  
</td>
      </tr>
    </table>
    <p>&nbsp;</p></td>
  </tr>
</table>

<?php  //Form close
  echo form_close();
?>         
 
 </div>
</body>
</html>
