  <!-- Start Content -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    
<script type="text/javascript">

qualified_url   =   "<?php echo $url; ?>";

//Datepicker block
$(document).ready(function() {
$( "#start_date" ).datepicker({
							dateFormat: 'dd-mm-yy',
							changeMonth: true,
							changeYear: true,
							showOn:"button",
                            maxDate: '0',
							buttonImage:base_url+'admin_asset/images/calendar-icon.png'
							});
$( "#end_date" ).datepicker({
							dateFormat: 'dd-mm-yy',
							changeMonth: true,
							changeYear: true,
							showOn:"button",
                            maxDate: '0',
							buttonImage:base_url+'admin_asset/images/calendar-icon.png'
							});
  
});

//receipt print report
function printReceiptList(){
	var start_date	    =	$('#start_date').val();
	var end_date	    =	$('#end_date').val();
    var filter_string   =   '';

	var start_string	=	start_date.replace(/\-/g,"");
	var end_string		=	end_date.replace(/\-/g,"");
    if((start_string.length>0) && (end_string.length >0)){
        var filter_string   =   '/'.concat(start_string).concat('-').concat(end_string);
    }

	//window.location		=	base_url.concat('admin/report/print-receipt').concat(filter_string);
    window.open (base_url.concat('admin/report/print-receipt').concat(filter_string),'_blank');
}

function filterReceipts(){
    var start_date  =   $('#start_date').val();
    var end_date    =   $('#end_date').val();
    if(start_date == ''){
        alert('Please enter a start date.');
        $('#start_date').focus();
        return false;
    }
    if(end_date == ''){
        alert('Please enter an end date.');
        $('#end_date').focus();
        return false;
    }
    
    var start_string    =   start_date.replace(/\-/g,"");
    var end_string      =   end_date.replace(/\-/g,"");

    window.location     =   qualified_url+start_string+'-'+end_string;
}
function clearFilter(){
    $('#start_date').val('');
    $('#end_date').val('');
    window.location     =   qualified_url.replace(/\-/g,"");
}


</script>

<!--  START Content -->
  <div class="container">
  <table width="650" border="0" align="center" cellpadding="0" cellspacing="0" style="text-align:left;">
    <tr>
        <td>
        Start Date: 
            <?php   $data = array(  'name'        =>'start_date',
                                    'id'          =>'start_date',
                                    'class'       =>'input-block',
                                    'value'       => $start_date,
                                    'readonly'    =>'readonly',
                                    'size'        =>'14'
                        );
                    echo form_input($data);?>

        End Date:
            <?php   $data = array(  'name'        =>'end_date',
                                    'id'          =>'end_date',
                                    'class'       =>'input-block',
                                    'value'       => $end_date,
                                    'readonly'    =>'readonly',
                                    'size'        =>'14'
                        );
                    echo form_input($data);?>

            <?php   $data = array(  'name'        =>'search_receipt',
                                    'id'          =>'search_receipt',
                                    'class'       =>'button-block',
                                    'content'     =>'Search',
                                    'onclick'     =>'filterReceipts();'
                        );
                    echo form_button($data);?>

            <?php   $data = array(  'name'        =>'clear_filter',
                                    'id'          =>'clear_filter',
                                    'class'       =>'button-block',
                                    'content'     =>'Clear Filter',
                                    'onclick'     =>'clearFilter();'
                        );
                    echo form_button($data);?>

            <?php   $data = array(  'name'        =>'print_receipt',
                                    'id'          =>'print_receipt',
                                    'class'       =>'button-block',
                                    'content'     =>'Print',
                                    'target'      => '_blank',
                                    'onclick'     =>'printReceiptList();'
                        );
                    echo form_button($data);?>                

        <br/>Recept Total: <?php echo $receptTotal;?>

        </td>
    </tr>
</table>
  

    <table width="650" border="0" align="center" cellpadding="0" cellspacing="0" style="text-align:left;">
    	<thead>
            <th>
                Receipt Number
            </th>
            <th>
                Receipt Date
            </th>
        	<th>
            	Receive From
            </th>
            <th>
            	Sum of rupees
            </th>
            <th>
            	Payment mode
            </th>
            <th>
            	On account of
            </th>
        </thead>
        <tbody>
    <?php
		if(!empty($results_details)){
			foreach($results_details as $key=>$value){
	?>
    	<tr>
            <td>
                <?php echo $results_details[$key]['id']; ?>
            </td>
            <td>
                <?php echo $this->etcommon->getUserDateForm($results_details[$key]['receipt_date']); ?>
            </td>

        	<td>
            	<?php echo $results_details[$key]['received_from']; ?>
            </td>
            <td>
            	<?php echo $results_details[$key]['sum_of_rupees']; ?>
            </td>
            <td>
            	<?php if($results_details[$key]['payment_mode']==1){
                    echo 'Cash'; 
                }else if($results_details[$key]['payment_mode']==2){
                     echo 'Cheque'; 
                }else if($results_details[$key]['payment_mode']==3){
                     echo 'Draft'; 
                }?>
            </td>
            <td>
            	<?php echo $results_details[$key]['c_or_d_account_of']; ?>
            </td>
            
        </tr>
    <?php
			}
		}else{
	?>
    	
    	<tr><td colspan="6">No results found!</td></tr>
     <?php
	 	}
	 ?>
    	</tbody>
    </table>
    <div class="pagination"><?php echo $pagination_links;?></div>
</div>

  <!--  END Content -->