  <!-- Start Content -->
<script type="text/javascript">

$(document).ready(function() {
  printReport();
  
});

function printReport(){
    document.body.offsetHeight;
    window.print();
}

</script>

<!--  START Content -->
<div class="container">

  <table bgcolor="#FFFFFF" width="650" border="0" align="center" cellpadding="0" cellspacing="0" style="text-align:left;">
    <tr>
        <td>
            <strong>
                <?php if(!empty($start_date)) echo ('Start Date : '. $start_date . ' <br/>'); ?>
                <?php if(!empty($end_date)) echo ('End Date : '. $end_date . ' <br/>'); ?>
                <?php if(!empty($receptTotal)) echo ('Recept Total : ' . $receptTotal); ?>
            </strong>
        </td>
    </tr>
</table>

    <table bgcolor="#FFFFFF" width="650" border="0" align="center" cellpadding="0" cellspacing="0" style="text-align:left;">
    	<thead>
            <th>
                Receipt Number
            </th>
            <th>
                Receipt Date
            </th>
        	<th>
            	Receive From
            </th>
            <th>
            	Sum of rupees
            </th>
            <th>
            	Payment mode
            </th>
            <th>
            	On account of
            </th>
        </thead>
        <tbody>
    <?php
		if(!empty($results_details)){
			foreach($results_details as $key=>$value){
	?>
    	<tr>
            <td>
                <?php echo $results_details[$key]['id']; ?>
            </td>
            <td>
                <?php echo $this->etcommon->getUserDateForm($results_details[$key]['receipt_date']); ?>
            </td>

        	<td>
            	<?php echo $results_details[$key]['received_from']; ?>
            </td>
            <td>
            	<?php echo $results_details[$key]['sum_of_rupees']; ?>
            </td>
            <td>
            	<?php if($results_details[$key]['payment_mode']==1){
                    echo 'Cash'; 
                }else if($results_details[$key]['payment_mode']==2){
                     echo 'Cheque'; 
                }else if($results_details[$key]['payment_mode']==3){
                     echo 'Draft'; 
                }?>
            </td>
            <td>
            	<?php echo $results_details[$key]['c_or_d_account_of']; ?>
            </td>
            
        </tr>
    <?php
			}
		}else{
	?>
    	
    	<tr><td colspan="6">No results found!</td></tr>
     <?php
	 	}
	 ?>
    	</tbody>
    </table>
</div>    
  <!--  END Content -->