<?php
class M_recept extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
        
    }
    const TABLE =	'tbl_recept';
	
	// -----------------------------------------------------------------------------------------
	/**
	*This function 'll insert recept details.
	*
	* @access public
	* @return $insert_id 
	*/	
	
    function insert($dataArr){
		$query		=	$this->db->insert(self::TABLE,$dataArr);
		$insert_id	=	$this->db->insert_id();
		return $insert_id;
    }
	
	
	// -----------------------------------------------------------------------------------------
	/**
	*This function 'll get recept details based on Id.
	*
	* @access public
	* @param Integer $recept_id
	* @return $result as array 
	*/	
	
    function getReceptById($recept_id=0){
		$recordArr		=	array();
		$this->db->select("*");
		$this->db->from(self::TABLE);
		$this->db->where(self::TABLE.'.id',$recept_id);
		$this->db->limit(1);
		$query 		=	$this->db->get();
		$result 	=	$query->result();
		foreach($result as $recept){
			$data		= array(	'id'					=>	$recept->id,
									'received_from'			=>	$recept->received_from,
									'sum_of_rupees'			=>	$recept->sum_of_rupees,
									'payment_mode'			=>	$recept->payment_mode,
									'c_or_d_num'			=>	$recept->c_or_d_num,
									'c_or_d_of'				=>	$recept->c_or_d_of,
									'c_or_d_date'			=>	$recept->c_or_d_date,
									'c_or_d_account_of'		=>	$recept->c_or_d_account_of,	
									'insert_date'			=>	$recept->insert_date	
			);
				
		
		}
		
		return $data;
    }	
	// -----------------------------------------------------------------------------------------
	
	
	/**
	*This function 'll get recept list according to dates.
	*
	* @access public
	* @param date $start_date
	* @param date $end_date
	* @param integer $offset
	* @param integer $limit
	* @return $result as array 
	*/	
	
	function getReceipsList($start_date='',$end_date='',$offset=0,$limit=0){
		$this->db->select();
		
		if($start_date != '' && $end_date != ''){
			$this->db->where('receipt_date >=',$start_date);
			$this->db->where('receipt_date <=',$end_date);
		}	
		$this->db->order_by('id', 'DESC');	
		$this->db->limit($limit,$offset);
		$result	=	$this->db->get(self::TABLE);
				
		$resultArr	=	array();
		foreach($result->result_array() as $value){
			extract($value);
			$tempArr	=	array();
			$tempArr['id']	=	$id;
			$tempArr['received_from']		=	$received_from;
			$tempArr['sum_of_rupees']		=	$sum_of_rupees;
			$tempArr['payment_mode']		=	$payment_mode;
			$tempArr['c_or_d_num']			=	$c_or_d_num;
			$tempArr['c_or_d_of']			=	$c_or_d_of;
			$tempArr['c_or_d_date']			=	$c_or_d_date;
			$tempArr['c_or_d_account_of']	=	$c_or_d_account_of;
			$tempArr['receipt_date']		=	$receipt_date;
			
			array_push($resultArr,$tempArr);
		}
		
		return $resultArr;
	}
	// -----------------------------------------------------------------------------------------
	

   /**
	*This function 'll get recept total according to filters.
	*
	* @access 	public
	* @param 	date 	$start_date
	* @param 	date 	$end_date
	* @return 	float 	$receptTotal  
	*/	
	
	function getReceptTotal($start_date='',$end_date=''){
		
		$arrReceptTotal	=array();
		$this->db->select_sum('sum_of_rupees');
		if($start_date != '' && $end_date != ''){
			$this->db->where('receipt_date >=',$start_date);
			$this->db->where('receipt_date <=',$end_date);
		}		
		$result	=	$this->db->get(self::TABLE);
		$arrReceptTotal =	$result->result_array();
		if(!empty($arrReceptTotal)){

			return $arrReceptTotal[0]['sum_of_rupees'];
		}
		else return 0;
	}
	// -----------------------------------------------------------------------------------------
	
	
   /**
	*	This function 'll get count of recept list according to dates.
	*
	* 	@access public
	* 	@param 	date $start_date
	* 	@param 	date $end_date
	* 	@return $count as integer 
	*/	
	
	function getReceipsCount($start_date='',$end_date=''){
		$this->db->select();
		if($start_date != '' && $end_date != ''){
			$this->db->where('receipt_date >=',$start_date);
			$this->db->where('receipt_date <=',$end_date);
		}

		$result	=	$this->db->get(self::TABLE);
		
		$count	=	$result->num_rows();		
		
		return $count;
	}
	
	
}
?>