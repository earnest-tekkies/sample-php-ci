<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {

	/**
	 * constructor
	 *
	 */
	function __construct() {
        parent::__construct();
		$this->load->language($this->config->item('admin_language'), $this->config->item('admin_language'));
		
		// setting current controller to the template class
		$this->template->set('controller', $this);
				
		$this->load->model('m_recept');	
		
		//if not logged in as admin/supporter then redirect to log in page
		if(!($this->quickauth->is_admin() || $this->quickauth->is_supportor())){
			$this->quickauth->redirect_out_admin();
		}
    }
	
	// -----------------------------------------------------------------------------------------
	
	
	/**
	 * function to display report list page
	 *
	 * @access public
	 * @return view 	report lists
	 */
	
	public function index(){
		$data['error'] 					=	FALSE;
		$this->template->set('admin_page_title', $this->lang->line('pgt_prefix').'Report');
		$this->template->load_partial('admin/admin_template', 'admin/index',$data);		
		
	}

	/**
	 * function to display list the recept by page
	 *
	 * 	@access public
	 *	@param 	string 	$str_filter
	 *	@param 	int 	$page
	 * 	@return view 	recept lists
	 */
	
	public function receptList($str_filter='00-00' , $page=0){
		
		$data 			=	array();	
		$start_date		=	'';
		$end_date		=	'';
		$receptTotal 	=	0;
		
		if($str_filter != '00-00'){
			$dateArr	=	explode('-',$str_filter);
			$start		=	$dateArr[0];
			$end		=	$dateArr[1];
			
			$st_day		=	substr($start,0,2);
			$st_mnth	=	substr($start,2,2);
			$st_year	=	substr($start,4,4);
			$start_date	=	$st_year.'-'.$st_mnth.'-'.$st_day;
			
			$end_day	=	substr($end,0,2);
			$end_mnth	=	substr($end,2,2);
			$end_year	=	substr($end,4,4);
			$end_date	=	$end_year.'-'.$end_mnth.'-'.$end_day;
		}
		
		//Pgination settings		
		$this->load->library('pagination');
		$config['base_url'] 		=	base_url().'admin/report/recepts-'.$str_filter;
		$config['total_rows'] 		=	$this->m_recept->getReceipsCount($start_date,$end_date);
		$config['per_page'] 		=	$this->config->item('admin_per_page');
		$config['uri_segment'] 		=	4; 		
		$this->pagination->initialize($config); 		
		$pagination_links			=	$this->pagination->create_links();
		
		$data['url']				=	base_url().'admin/report/recepts-';
		$data['pagination_links']	=	$pagination_links;
		$data['start_date']			=	$start_date !='' ? date('d-m-Y',strtotime($start_date)) : '';
		$data['end_date']			=	$end_date != '' ? date('d-m-Y',strtotime($end_date)) : '';
		
		//Get receips list according to the dates preferred
		$receipsListArr				=	$this->m_recept->getReceipsList($start_date,$end_date,$page,$config['per_page']);
		$data['results_details']	=	$receipsListArr;
		$receptTotal 				=	$this->m_recept->getReceptTotal($start_date,$end_date);
		$data['receptTotal']		=	$receptTotal;
		$this->template->set('admin_page_title', $this->lang->line('pgt_prefix').'Report :: Recept List');
		$this->template->load_partial('admin/admin_template', 'admin/recept_list',$data);		
		
	}

	/**
	 * function print reports based on the report type and date range
	 *
	 * 	@access public
	 *	@param 	string 	$report_type
	 * 	@return string 	$str_filter
	 */
	
	public function printReport($report_type='', $str_filter='00-00'){
		
		# code to execute the print report  process
		if(!is_null($report_type)){

			# if report type is not null switch to print
			switch ($report_type) {

				case 'receipt':
					# code to execute print receipt process

					$data 			=	array();	
					$start_date		=	'';
					$end_date		=	'';
					$receptTotal 	=	0;
						
					if($str_filter != '00-00'){
						$dateArr	=	explode('-',$str_filter);
						$start		=	$dateArr[0];
						$end		=	$dateArr[1];
							
						$st_day		=	substr($start,0,2);
						$st_mnth	=	substr($start,2,2);
						$st_year	=	substr($start,4,4);
						$start_date	=	$st_year.'-'.$st_mnth.'-'.$st_day;
							
						$end_day	=	substr($end,0,2);
						$end_mnth	=	substr($end,2,2);
						$end_year	=	substr($end,4,4);
						$end_date	=	$end_year.'-'.$end_mnth.'-'.$end_day;
					}
					
					$data['start_date']			=	$start_date !='' ? date('d-m-Y',strtotime($start_date)) : '';
					$data['end_date']			=	$end_date != '' ? date('d-m-Y',strtotime($end_date)) : '';

					# get receips list according to the dates preferred
					$offset 					=	$this->m_recept->getReceipsCount($start_date,$end_date);
					$receipsListArr				=	$this->m_recept->getReceipsList($start_date,$end_date,'',$offset);
					$data['results_details']	=	$receipsListArr;
					$receptTotal 				=	$this->m_recept->getReceptTotal($start_date,$end_date);
					$data['receptTotal']		=	$receptTotal;

					$this->template->set('admin_page_title', $this->lang->line('pgt_prefix').'Print :: Recept List');
					$this->template->load_partial('admin/print_template', 'admin/print_recept_list',$data);		
					break;
				
				default:
					# code...
					break;

			}# end switch

		}# end if report type
	}
		

	
}
