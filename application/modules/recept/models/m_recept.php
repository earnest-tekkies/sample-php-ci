<?php
class M_recept extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
        
    }
    const TABLE =	'tbl_recept';
	
	// -----------------------------------------------------------------------------------------
	/**
	*This function 'll insert recept details.
	*
	* @access public
	* @return $insert_id 
	*/	
	
    function insert($dataArr){
		$query		=	$this->db->insert(self::TABLE,$dataArr);
		$insert_id	=	$this->db->insert_id();
		return $insert_id;
    }
	
	
	// -----------------------------------------------------------------------------------------
	/**
	*This function 'll get recept details based on Id.
	*
	* @access public
	* @param Integer $recept_id
	* @return $result as array 
	*/	
	
    function getReceptById($recept_id=0){
		$recordArr		=	array();
		$this->db->select("*");
		$this->db->from(self::TABLE);
		$this->db->where(self::TABLE.'.id',$recept_id);
		$this->db->limit(1);
		$query 		=	$this->db->get();
		$result 	=	$query->result();
		foreach($result as $recept){
			$data		= array(	'id'					=>	$recept->id,
									'received_from'			=>	$recept->received_from,
									'sum_of_rupees'			=>	$recept->sum_of_rupees,
									'payment_mode'			=>	$recept->payment_mode,
									'c_or_d_num'			=>	$recept->c_or_d_num,
									'c_or_d_of'				=>	$recept->c_or_d_of,
									'c_or_d_date'			=>	$recept->c_or_d_date,
									'c_or_d_account_of'		=>	$recept->c_or_d_account_of,	
									'receipt_date'			=>	$recept->receipt_date,	
									'insert_date'			=>	$recept->insert_date	
			);
				
		
		}
		
		return $data;
    }
	
	
}
?>