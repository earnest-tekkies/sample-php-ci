	<!-- Start Content -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<script type="text/javascript">

$(document).ready(function() {

  //Date picker instantiation block
  $( "#c_or_d_date" ).datepicker({
    dateFormat: 'dd-mm-yy',
    changeMonth: true,
    changeYear: true,
    showOn:"button",
    maxDate: '0',
    buttonImage:base_url+'admin_asset/images/calendar-icon.png'
  });
  
  $( "#receipt_date" ).datepicker({
    dateFormat: 'dd-mm-yy',
    changeMonth: true,
    changeYear: true,
    showOn:"button",
    maxDate: '0',
    buttonImage:base_url+'admin_asset/images/calendar-icon.png'
  });

  //paytype management block
  $('.pay-switch').hide();

  $('#payment_mode').change(function(){

    var payment=$('#payment_mode').val();

    if(payment==1){
      $('.pay-switch').hide();
    }else if(payment==2){
       $('.pay-switch').show();
       $('#td-c-num').html('Cheque number');
       $('#td-c-of').html('Cheque of');
       $('#td-c-date').html('Cheque date');
       
       
    }else if(payment==3){
       $('.pay-switch').show();
       $('#td-c-num').html('Draft number');
       $('#td-c-of').html('Draft of');
       $('#td-c-date').html('Draft date');
    }
  });
  
});
</script>
	<div class="container">
    
<?php $attributes = array('name' => 'new_recept', 'id' => 'new_recept', 'method'=> 'post');?>
<?php echo form_open('admin/new-recept',$attributes);?>
    <table id="tbl-messages" width="500" border="0" align="center" cellpadding="0" cellspacing="0" style="text-align:left; display:none;">
      <tr>
        <td id="td-msg-container">
        </td>
      </tr>
    </table>

    <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" style="text-align:left;">
        <tr><br/>
        <td width="247" class="ssssss"> <?php echo $this->lang->line('received_from'); ?> </td>
        <td width="253"><label for="textfield"></label>
          
          <?php $data = array('name'			=>'received_from',
              								'id'           =>'received_from',
													   'class'		=>'input-block',
													   'placeholder' 	=>'From');
                                 	  echo form_input($data);?>
                                 <?php echo form_error('received_from'); ?>

          </td>
      </tr>
      <tr>
        <td><?php echo $this->lang->line('sum_of_rupees'); ?></td>
        <td>
			<?php $data = array('name'			=>'sum_of_rupees',
              						'id'        =>'sum_of_rupees',
													 'class'		=>'input-block',
													 'value' 		=>'',
                          'placeholder'  =>'0.00'
                           );
        echo form_input($data);?>
<?php echo form_error('sum_of_rupees'); ?>
        </td>
      </tr>
      <tr>
        <td><?php echo $this->lang->line('payment_mode'); ?></td>
        <td><label for="select"></label>
          <select name="payment_mode" id="payment_mode">
            <!--<option value="">Select</option>-->
            <option select="select" value="1">Cash</option>
            <option value="2">Cheque</option>
            <option value="3">Draft</option>

          </select>
        <?php echo form_error('payment_mode'); ?>
      </td>
      </tr>
      <tr class="pay-switch">
        <td id="td-c-num"><?php echo $this->lang->line('cheque_number'); ?></td>
        <td>
        <?php $data = array('name'		=>'c_or_d_num',
              							'id'      =>'c_or_d_num',
													 'class'		=>'input-block',
													 'placeholder' 	=>'Number');
                                 	  echo form_input($data);?>
                                 <?php echo form_error('c_or_d_num'); ?>

        </td>
      </tr>
      <tr class="pay-switch">
        <td id="td-c-of"><?php echo $this->lang->line('cheque_of'); ?></td>
        <td><?php $data = array('name'			=>'c_or_d_of',
              									'id'        =>'c_or_d_of',
													      'class'		  =>'input-block',
													      'placeholder' 	=>'');
                  echo form_input($data);?>
            <?php echo form_error('c_or_d_of'); ?>
                             </td>
      </tr>
      <tr class="pay-switch">
        <td id="td-c-date"><?php echo $this->lang->line('cheque_date'); ?></td>
        <td>
        <?php $data = array('name'			=>'c_or_d_date',
              							'id'           =>'c_or_d_date',
													 'class'		=>'input-block',
													 'placeholder' 	=>'dd-mm-yyyy',
                           'readonly' =>'readonly');
                                 	  echo form_input($data);?>
                                 <?php echo form_error('c_or_d_date'); ?>
                             </td>
      </tr>

      <tr>
        <td><?php echo $this->lang->line('on_account_of'); ?> </td>
        <td><?php $data = array('name'			=>'c_or_d_account_of',
              									'id'           =>'c_or_d_account_of',
													       'class'		=>'input-block',
													       'placeholder' 	=>'');
                                 	 echo form_input($data);?>
                                 <?php echo form_error('c_or_d_account_of'); ?></td>
                                 
      </tr>

      <tr>
        <td id="td-receipt-date"><?php echo $this->lang->line('receipt_date'); ?></td>
        <td>
        <?php $data = array('name'      =>'receipt_date',
                            'id'        =>'receipt_date',
                           'class'    =>'input-block',
                           'value'  =>date('d-m-Y'),
                           'readonly' =>'readonly');
                                    echo form_input($data);?>
                                 <?php echo form_error('receipt_date'); ?>
                             </td>
      </tr>

      <tr>
        <td>&nbsp;</td>
        <td>
          <?php 
          $data = array('name'    =>  'Add_recept',
                        'id'      =>  'Add_recept',
                        'value'   =>  'Print',
                        'class'   =>  'submit-block'
                      );
          echo form_submit($data); 
        ?>
      </tr>
  </table>
<?php echo form_close(); ?>
		
			<!--</form>-->
			<!-- End Form  -->
		</div>

	<!--  End Content -->