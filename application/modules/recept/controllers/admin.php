<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {

	/**
	 * constructor
	 *
	 */
	function __construct() {
        parent::__construct();
		$this->load->language($this->config->item('admin_language'), $this->config->item('admin_language'));
		
		// setting current controller to the template class
		$this->template->set('controller', $this);
				
		$this->load->model('m_recept');	
		
		//if not logged in as admin/supporter then redirect to log in page
		if(!($this->quickauth->is_admin() || $this->quickauth->is_supportor())){
			$this->quickauth->redirect_out_admin();
		}
    }
	
	// -----------------------------------------------------------------------------------------
	
	
	/**
	 * function to display receptAdd page
	 *
	 * 	@access public
	 *	@param 	array 	recept details
	 * 	@return view 	members_add page
	 */
	
	public function receptAdd(){

		$data['error'] 			=	FALSE;
		$data['error_flag']		=	false;
		$data['recept_type'] 	=	'';
		$payment_mode			=	trim($this->input->post('payment_mode'));
		if($payment_mode!=1){

			$rules = array(
						array(
							'field'   => 'received_from',
							'label'   => 'lang:received_from',
							'rules'   => 'trim|required|',
						),
						array(
							'field'   => 'sum_of_rupees',
							'label'   => 'lang:sum_of_rupees',
							'rules'   => 'trim|required|integer',
						),
						array(
							'field'   => 'payment_mode',
							'label'   => 'lang:payment_mode',
							'rules'   => 'trim|required',
						),
						array(
							'field'   => 'c_or_d_num',
							'label'   => 'lang:cheque_number',
							'rules'   => 'trim|required',
						),
						array(
							'field'   => 'c_or_d_of',
							'label'   => 'lang:cheque_of',
							'rules'   => 'trim|required',
						),
						array(
							'field'   => 'c_or_d_date',
							'label'   => 'lang:cheque_date',
							'rules'   => 'trim|required',
						),
						array(
							'field'   => 'c_or_d_account_of',
							'label'   => 'lang:on_account_of',
							'rules'   => 'trim|required',
						),
						array(
							'field'   => 'receipt_date',
							'label'   => 'lang:receipt_date',
							'rules'   => 'trim|required',
						)
					);
		}else{

			$rules = array(
						array(
							'field'   => 'received_from',
							'label'   => 'lang:received_from',
							'rules'   => 'trim|required|',
						),
						array(
							'field'   => 'sum_of_rupees',
							'label'   => 'lang:sum_of_rupees',
							'rules'   => 'trim|required|integer',
						),
						array(
							'field'   => 'payment_mode',
							'label'   => 'lang:payment_mode',
							'rules'   => 'trim|required',
						),
						array(
							'field'   => 'c_or_d_account_of',
							'label'   => 'lang:on_account_of',
							'rules'   => 'trim|required',
						),
						array(
							'field'   => 'receipt_date',
							'label'   => 'lang:receipt_date',
							'rules'   => 'trim|required',
						)
					);	
		}	
			
		
		
		$this->template->set('admin_page_title', $this->lang->line('pgt_prefix').'New Recept');
		//$data['group_options']				=	$this->userGroupOptionsHtml();
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules($rules);
		$this->form_validation->set_error_delimiters('<span class="error-message">','</span>');
		
		$data['post']=$this->input->post();
		if ($this->form_validation->run() != FALSE){
			
			$received_from			=	trim($this->input->post('received_from'));
			$sum_of_rupees			=	trim($this->input->post('sum_of_rupees'));		
			$payment_mode			=	trim($this->input->post('payment_mode'));
			$c_or_d_account_of		=	trim($this->input->post('c_or_d_account_of'));
			$receipt_date			=	$this->etcommon->getDbDateForm(trim($this->input->post('receipt_date')));
			
			if($payment_mode!=1){
				$c_or_d_num			=	trim($this->input->post('c_or_d_num'));
				$c_or_d_of			=	trim($this->input->post('c_or_d_of'));		
				$c_or_d_date		=	$this->etcommon->getDbDateForm(trim($this->input->post('c_or_d_date')));
			}else{
				$c_or_d_num			=	"";
				$c_or_d_of			=	"";		
				$c_or_d_date		=	"";
			}
			
			//if all entered data are valid then proceed to insertion
			if($data['error'] != TRUE){
				$insertArr	=array(
						'received_from'			=>$received_from,
						'sum_of_rupees'			=>$sum_of_rupees,
						'payment_mode'			=>$payment_mode,
						'c_or_d_num'			=>$c_or_d_num,
						'c_or_d_of'				=>$c_or_d_of,
						'c_or_d_date'			=>$c_or_d_date,
						'c_or_d_account_of'		=>$c_or_d_account_of,
						'receipt_date'			=>$receipt_date,
						'insert_date'			=>date('Y-m-d')		
						);
				$recept_id	=	$this->m_recept->insert($insertArr);
				redirect('admin/print-recept/'.$recept_id);			
				
			}
				
		}else{
			$data['error_flag']		=	true;
			$data['recept_type']	=	$payment_mode;
		}
		$this->template->load_partial('admin/admin_template', 'admin/recept_add',$data);		
		
	}

	/**
	 * printrecept function 'll print the recept by recept id
	 *
	 * 	@access public
	 *	@param 	int 	$recept_id
	 * 	@return view 	recept print view
	 */

	function printRecept($recept_id){

		$receptArr	=	$this->m_recept->getReceptById($recept_id);

		$this->template->set('admin_page_title', $this->lang->line('pgt_prefix').'Print Recept');

		$receptArr['receipt_date']	=	$this->etcommon->getUserDateForm($receptArr['receipt_date']);
		$receptArr['c_or_d_date']	=	$this->etcommon->getUserDateForm($receptArr['c_or_d_date']);

		$this->template->load_partial('admin/print_template', 'admin/recept_print',$receptArr);
	}
	
}
