<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {

	function __construct() {
        parent::__construct();
		$this->load->language($this->config->item('admin_language'), $this->config->item('admin_language'));
		// setting current controller to the template class
		$this->template->set('controller', $this);
		//setting currently selected main menu
		$this->template->set('active_menu', 'dashboard');

		$this->load->model('user_model');
		if(!($this->quickauth->is_admin() || $this->quickauth->is_supportor())){
			$this->quickauth->redirect_out_admin();
		}		
    }
	
	public function index(){
		//setting current page title
		$this->template->set('admin_page_title', $this->lang->line('admin_page_title'));
		
		$pr_total_profit = 0;
		$pr_average_profit_per_month = 0;
		$pr_current_month_profit = 0;
		
		$af_total_profit = 0;
		$af_average_profit_per_month = 0;
		$af_current_month_profit = 0;
		
		$data	=	array();
		$this->template->set('admin_page_title', $this->lang->line('pgt_prefix').'Dashboard');
		$this->template->load_partial('admin/admin_template', 'index',$data);
	}
}
