<?php
class M_configuration extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
	
	// -----------------------------------------------------------------------------------------
	
	/**
	*This function 'll get pdfs protection configuration details from respective database table.
	* @return the pdf protection details record.
	*/ 
    
    function getPDFConfig(){
		$records=array();
		$this->db->select('*');
	 	$query=$this->db->get('sm_pdf_protect_config');
		$result = $query->result();
		foreach($result as $str){
			$datas=array('font_size'=>$str->font_size,'font_color'=>$str->font_color,'stamping_position'=>$str->stamping_position,'stamping_data'=>$str->stamping_data);
			array_push($records,$datas);
		}
		return $records;
    }
	
	// -----------------------------------------------------------------------------------------
	
	/**
	*This function 'll update pdfs protection configuration details in respective database table.
	*/ 
	
	function updatePDFConfig($updatepdfProtectionArr){
		$rowCount=count($this->getPDFConfig());
		//insert the Pdf config details if there is no such row otherwise update the details.
		if($rowCount > 0){
			$result=$this->db->update('sm_pdf_protect_config',$updatepdfProtectionArr);
		}
		else {
			$result=$this->db->insert('sm_pdf_protect_config',$updatepdfProtectionArr);
		}
		
	}
	
	// -----------------------------------------------------------------------------------------
	
	/**
	*This function 'll get automatic emails details from respective database table.
	* @return the automatic email details record.
	*/ 
	
	function getautomaticEmails($type=0,$language=1){
		$records=array();
		$this->db->select('*');
		if($type > 0){
			$this->db->where('type' ,$type);
		}
		$this->db->where('language' ,$language);
	 	$query=$this->db->get('sm_automatic_emails');
		$result = $query->result();
		foreach($result as $str){
			$datas=array('type'=>$str->type,'enabled'=>$str->enabled,'subject'=>$str->subject,'message'=>$str->message,'days_before_expire'=>$str->days_before_expire);
			array_push($records,$datas);
		}
		return $records;
    }
	
	// -----------------------------------------------------------------------------------------
	
	/**
	*This function 'll get automatic emails details from respective database table.
	* @return the number of records.
	*/ 
	
	function checkAutomaticEmailTemplateExists($templateId,$language_id=1){
		$this->db->select('*');
		$this->db->where('type' ,$templateId);
		$this->db->where('language' ,$language_id);
		$query=$this->db->get('sm_automatic_emails');
		if($query->num_rows()==1){			
			return true;
		}else{
			return false;	
		}
		
	}
	
	// -----------------------------------------------------------------------------------------
	
	/**
	*This function 'll add automatic emails details in respective database table.
	*/ 
	
	function addAutomaticEmailDetails($automaticEmailDetails){
		$type			=	intval($automaticEmailDetails['type']);
		$language_id	=	intval($automaticEmailDetails['language']);
		//insert the automatic email details if there is no such record otherwise update the details.
		if($this->checkAutomaticEmailTemplateExists($type,$language_id)){
			$this->db->where('type' ,$type);
			$result=$this->db->update('sm_automatic_emails',$automaticEmailDetails);
		}else{
			$result=$this->db->insert('sm_automatic_emails',$automaticEmailDetails);
		}
		
	}
}
?>