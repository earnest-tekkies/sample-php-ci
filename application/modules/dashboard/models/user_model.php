<?php
class User_model extends CI_Model {

    private $main_table = 'tbl_user';

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

	function search($options = array()){
		if(isset($options['select'])){
			$this->db->select($options['select']);
		}
		if(isset($options['select_sum'])){
			foreach($options['select_sum'] as $select_sum_field){
				$this->db->select_sum($select_sum_field);
			}
		}
		if(isset($options['where'])){
			foreach($options['where'] as $key_where => $key_value){
				$this->db->where($key_where, $key_value);
			}
		}
		if(isset($options['where_in'])){
			foreach($options['where_in'] as $field => $val){
				$this->db->where_in($field, $val);
			}
		}
		if(isset($options['limit'])){
			if(isset($options['offset'])){
				$this->db->limit($options['limit'], $options['offset']);	
			}else{
				$this->db->limit($options['limit']);	
			}
		}
		if(isset($options['sort'])){
			foreach($options['sort'] as $sort_field => $sort_type){
				$this->db->order_by($sort_field, $sort_type);
			}
		}else if(isset($this->position_field)){
			$this->db->order_by($this->position_field, 'asc');
		}
		
		if(isset($options['group_by'])){
			foreach($options['group_by'] as $group_by){
				$this->db->group_by($group_by);
			}
		}
		
		if(isset($options['row_array'])){
			return $this->db->get($this->main_table)->row_array();
		}else if(isset($options['row_object'])){
			return $this->db->get($this->main_table)->row_object();
		}else if(isset($options['result_array'])){
			return $this->db->get($this->main_table)->result_array();
		}else{
			return $this->db->get($this->main_table)->result();	
		}
	}
}
?>