<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends MX_Controller {
	
	//sets rules for login form validation	
	private $rules = array(
						array(
							'field'   => 'username',
							'label'   => 'lang:email',
							'rules'   => 'trim|required|valid_email',
						),
						array(
							'field'   => 'password',
							'label'   => 'lang:password',
							'rules'   => 'trim|required|min_length[6]',
						)
					);		
	
	/**
	 * constructor
	 *
	 */
	function __construct() {
        parent::__construct();
		$this->load->language($this->config->item('admin_language'), $this->config->item('admin_language'));
		$this->load->model('dashboard/m_configuration');
	}
	
	// -----------------------------------------------------------------------------------------
	
	/**
	 * This function 'll load the login page
	 *
	 */
	function index(){
		$this->login();
	}
	
	// -----------------------------------------------------------------------------------------
	
	/**
	 * This function 'll load the login page
	 *
	 */
	function maintenance(){
		$this->load->view('index/maintenance');
	}
	// -----------------------------------------------------------------------------------------
	
	/**
	 * This function 'll do login process
	 *
	 * @access public
	 * @return redirect to users home if success else shows the login page
	 */
	
	function login(){
		
		$this->quickauth->is_maintain();
		
		$data['error'] = FALSE;
		
		//get welcome page pretty url
		$this->load->model('m_config');	
		$welcome_pretty_url	=	'';
		$language_id		=	$this->quickauth->_get_language_code();
		$rpDataArr			=	$this->m_config->getItemWithWhere('tbl_welcome', array('language'=>$language_id,'enabled'=>1));
		if(!empty($rpDataArr)){
			foreach($rpDataArr as $key=>$value){
				$welcome_pretty_url	=	$rpDataArr[$key]->pretty_url;
			}
		}	
					
		//if not logged in show login window
		if ($this->session->userdata('logged_in') == FALSE){
			
			$data['error'] = FALSE;
			$this->load->library('form_validation');
			$this->form_validation->set_rules($this->rules);
			$this->form_validation->set_error_delimiters('<span class="error-message">','</span>');
			
			if ($this->form_validation->run() != FALSE){
				$query		=		$this->db->get_where('tbl_user',
													array(
														'email'		=> 	$this->input->post('username'),
														'password' 	=>	md5($this->input->post('password'))
														)
												  );
				//if user exists								  
				if ($query->num_rows() === 1) {
					$result		=	$query->row();
					
					//for user session
					$dataArr	=	array(
										'logged_in' => TRUE,
										'username' => $result->name,
										'user_id' => $result->id,
										'name' => $result->name,
										'group_id' => $result->group_id
									  );
									  
					//if admin user, then set the sesion and redirect to dashboards page
					if($result->group_id	==	1){
						$this->session->set_userdata($dataArr);
						redirect('admin/dashboard');	
						
					}

				}else{
					//invalid user
					$data['error'] = TRUE;
					$data['error_message']	=	$this->lang->line('access_denied');
				}
				
			}
		}else{			
			// if admin user, then redirect to dashboards page
			if($this->session->userdata('group_id') == 1){
				redirect('admin/dashboard');	
			}			
		}
		$this->load->view('index/login',$data);
	}
	
	// -----------------------------------------------------------------------------------------
	
	/**
	 * This function 'll do logout process and redirect to login page
	 *
	 * @access public
	 * 
	 */
	
	function logout(){
		$this->session->sess_destroy();
		redirect('user/login');
	}
	
	// -----------------------------------------------------------------------------------------
	
	/**
	 * This function 'll create password string
	 *
	 * @access private
	 * @return String password
	 * 
	 */
	
	private function create_new_password(){
		$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$str = '';
		for ($i = 0; $i < 8; $i++){
			$str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
		}
		return $str;
	}
	
	// -----------------------------------------------------------------------------------------
	
	private function sendmail($to,$from,$subject,$body){
		$to="$to";
		$headers = "MIME-Version: 1.0\r\n";
		$headers.= "Content-type: text/html; ";
		$headers.= "charset=iso-8859-1\r\n";
		$headers.= "From: $from";
		$subject = "$subject";
		$body = "<HTML><BODY>$body</BODY></HTML>";
				
		if(mail($to,$subject,$body,$headers)){
			return true;
		}else{
			return false;
		}		
	}
	
	public function replacePlaceHolders($string, $vars_ar) {
      foreach($vars_ar as $k => $v) {
          $search[] = '[' . $k . ']';
          $replace[] = $v;
      }
      return str_replace($search, $replace, $string);		
		
	}
}

?>